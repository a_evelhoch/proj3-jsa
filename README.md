# PROJ3-JSA README 

Author: Andrew Evelhoch
Contact: aevelhoc@uoregon.edu

## DESCRIPTION
### The goals of this project are:

* Familiarize myself with AJAX by implementing an AJAX-style automatic updating for the given vocab.html game

### Functionality:

Once the bitbucket directory is cloned to a machine, one simply needs to run the run.sh script and it will build and run the server. While it is running, once can then open a web browser and load up the anagram vocab game by going to localhost:5000. The game will load up a list of words and an assortment of random letters. The goal of the game is to pick out the words on the list that you can create with the jumble of letters. To get another jumble of letters, simply refresh the page. Once done with the game, you will need to docker stop it in terminal.

### What I did:

After looking at the static vocab.html + flask_vocab.py game that talks to the flask server through forms and the AJAX style minijax.html + flask_minjax.py and familiarizing myself with how they each work, I managed to update the vocab.html and flask_vocab.py files to make it work AJAX-style. I also updated the Dockerfile and edited the credentials.ini to turn in.
